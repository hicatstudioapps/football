package com.example.cq.football;

import android.graphics.Bitmap;

public class Link {
   private Bitmap favicon;
   private String title;
   private String url;

   public Link() {
   }

   public Link(String var1, Bitmap var2) {
      this.url = var1;
      this.favicon = var2;
   }

   public Bitmap getFavicon() {
      return this.favicon;
   }

   public String getTitle() {
      return this.title;
   }

   public String getUrl() {
      return this.url;
   }

   public void setFavicon(Bitmap var1) {
      this.favicon = var1;
   }

   public void setTitle(String var1) {
      this.title = var1;
   }

   public void setUrl(String var1) {
      this.url = var1;
   }
}
