package com.example.cq.football;

import java.io.IOException;
import java.net.CookieHandler;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by CQ on 22/03/2016.
 */
public class ServiceGenerator  {
//    private static final String BASE_URL = "http://benainslie-racing.qiupreview.com/";
    private static final String BASE_URL = "http://192.168.137.1/ben/";

             private static Retrofit.Builder builder =  new Retrofit.Builder()
                     .baseUrl(BASE_URL)
                     .addConverterFactory(GsonConverterFactory.create());

             private static OkHttpClient.Builder httpClient =
             new OkHttpClient.Builder();
             public static <S> S createService(Class<S> serviceClass) {
                 builder.client(httpClient.build());
                 httpClient.addInterceptor(new Interceptor() {
                     @Override
                     public Response intercept(Chain chain) throws IOException {

                         return null;
                     }
                 });
         Retrofit retrofit = builder.build();
         return retrofit.create(serviceClass);
         }
}
