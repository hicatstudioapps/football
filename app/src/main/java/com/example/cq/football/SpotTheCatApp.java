package com.example.cq.football;

//import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.soccer.footballresults.R;
//import com.nostra13.universalimageloader.core.ImageLoader;
//import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.io.File;

import badabing.lib.ServerUtilities;


public class SpotTheCatApp extends MultiDexApplication {


    public static  InterstitialAd interstitialAd;
    AdRequest request;
    public static Typeface light;
    public static Typeface bold;
    public static Typeface regular;
    public static SharedPreferences preferences;
    public static String store= Environment.getExternalStorageDirectory()+"/football";
    public static int id=2;
    public static String appsUrl="http://192.168.137.1/lock.json";
    public static IService service;

    public void onCreate() {
      super.onCreate();
        service= ServiceGenerator.createService(IService.class);
      preferences= getSharedPreferences("interistial",MODE_PRIVATE);
      interstitialAd=new InterstitialAd(this);
      interstitialAd.setAdUnitId("ca-app-pub-1147824671772507/1707091672");
       request= new AdRequest.Builder().build();
      interstitialAd.setAdListener(new AdListener() {
          @Override
          public void onAdClosed() {
              preferences= getSharedPreferences("interistial",MODE_PRIVATE);
              preferences.edit().putLong("date",new DateTime().getMillis()).commit();
              interstitialAd.loadAd(request);
          }
      });
      interstitialAd.loadAd(request);
//        if(getSharedPreferences("data",MODE_PRIVATE).getBoolean(Constants.CONTENT_READY,false))
   //   light = Typeface.createFromAsset(getAssets(),"din-light.ttf");
      bold = Typeface.createFromAsset(getAssets(),"bold.otf");
    //   regular = Typeface.createFromAsset(getAssets(),"Roboto-Regular.ttf");
       Intent shortcutIntent = new Intent(getApplicationContext(),
               MainActivity.class);

       shortcutIntent.setAction(Intent.ACTION_MAIN);
       //shortcutIntent is added with addIntent
       Intent addIntent = new Intent();
       addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
       addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
       addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
               Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                       R.mipmap.ic_launcher));

       addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
       // finally broadcast the new Intent
       getApplicationContext().sendBroadcast(addIntent);

   }

    public static void showInterstitial(){
        if(interstitialAd.isLoaded()){
            long date= preferences.getLong("date",0);
            DateTime current= new DateTime();
            DateTime old=null;
            if(date>0){
                old= new DateTime(date);
            }else {
                preferences.edit().putLong("date",new DateTime().getMillis()).commit();
                old= new DateTime();
            }
            //int minutes=new Period(old,current).getMinutes();
            if(old!=null && new Period(old,current).getMinutes()>2){
                Log.d("mostrar inerstial", "ghghg");

                interstitialAd.show();
            }
            else
                Log.d("no mostrar inerstial","ghghhh");
        }
    }
}
