package com.example.cq.football;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.soccer.footballresults.R;

public class Eula {
   static AlertDialog OptionDialog;
   private String EULA_PREFIX = "eula_";
   private AppCompatActivity mActivity;

   public Eula(AppCompatActivity var1) {
      this.mActivity = var1;
   }

   private PackageInfo getPackageInfo() {
      PackageInfo var1 = null;

      PackageInfo var2;
      try {
         var2 = this.mActivity.getPackageManager().getPackageInfo(this.mActivity.getPackageName(), 1);
      } catch (NameNotFoundException var3) {
         var3.printStackTrace();
         return var1;
      }

      var1 = var2;
      return var1;
   }

   private void goToUrl(String var1) {
      Intent var2 = new Intent("android.intent.action.VIEW", Uri.parse(var1));
      this.mActivity.startActivity(var2);
   }

   public void show() {
      PackageInfo var4 = this.getPackageInfo();
      final String var3 = this.EULA_PREFIX + var4.versionCode;
      final SharedPreferences var2 = PreferenceManager.getDefaultSharedPreferences(this.mActivity);
      boolean var1 = var2.getBoolean(var3, false);
      if(OptionDialog == null && !var1) {
         String var6 = this.mActivity.getString(R.string.app_name) + " vers. " + var4.versionName;
         Builder var5 = (new Builder(this.mActivity)).setTitle(var6).setMessage("User Cookie Consent Policy: \n\nPlease Read CAREFULLY! \n\nWe use device identifiers to personalise content and ads, to provide social media features and to analyse our traffic. We also share such identifiers and other information from your device with our social media, advertising and analytics partners.").setCancelable(false).setPositiveButton("I agree", new OnClickListener() {
            public void onClick(DialogInterface var1, int var2x) {
               Editor var3x = var2.edit();
               var3x.putBoolean(var3, true);
               var3x.commit();
               var1.dismiss();
            }
         }).setNeutralButton("See details", new OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
               Eula.OptionDialog = null;
               Eula.this.goToUrl("http://www.google.com/intl/en/policies/privacy/partners/");
            }
         }).setNegativeButton("Exit", new OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
               Eula.OptionDialog = null;
               Eula.this.mActivity.finish();
            }
         });
         var5.create();
         OptionDialog = var5.show();
      }

   }
}
