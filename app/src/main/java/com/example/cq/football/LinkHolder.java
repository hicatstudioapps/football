package com.example.cq.football;

import android.widget.ImageView;
import android.widget.TextView;

public class LinkHolder {
   private ImageView imageView;
   private TextView url;

   public ImageView getImageView() {
      return this.imageView;
   }

   public TextView getUrl() {
      return this.url;
   }

   public void setImageView(ImageView var1) {
      this.imageView = var1;
   }

   public void setUrl(TextView var1) {
      this.url = var1;
   }
}
