package com.example.cq.football;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CQ on 25/04/2016.
 */
public class App {
    public List<Apps> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<Apps> tagsList) {
        this.tagsList = tagsList;
    }

    @SerializedName("apps")
    @Expose
    private List<Apps> tagsList = new ArrayList<Apps>();
}
