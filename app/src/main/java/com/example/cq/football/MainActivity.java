package com.example.cq.football;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Build.VERSION;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebIconDatabase;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.lacostra.utils.notification.Download.DowloadManager;
import com.lacostra.utils.notification.NetWork;
import com.lacostra.utils.notification.PackageUtil;
import com.lacostra.utils.notification.Permission.Permission;
import com.lacostra.utils.notification.Permission.PermissionActivity;
import com.soccer.footballresults.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("ResourceType")
public class MainActivity extends PermissionActivity {
   public static final String ARXVP = "com.arx.arxplayer";
   public static final String ARXVP_PLAYBACK_CLASS = "com.arx.arxplayer.VideoPlayerActivity";
   public static final String EXTRA_DECODE_MODE = "decode_mode";
   public static final String EXTRA_HEADERS = "headers";
   public static final String EXTRA_POSITION = "position";
   public static final String EXTRA_RETURN_RESULT = "return_result";
   public static final String EXTRA_SUBTITLES = "subs";
   public static final String EXTRA_SUBTITLES_ENABLE = "subs.enable";
   public static final String EXTRA_TITLE = "title";
   public static final String EXTRA_VIDEO_LIST = "video_list";
   public static final String KEY_TITLE = "android.media.metadata.TITLE";
   public static final String MXVP = "com.mxtech.videoplayer.ad";
   public static final String MXVP_PLAYBACK_CLASS = "com.mxtech.videoplayer.ad.ActivityScreen";
   public static final String MXVP_PRO = "com.mxtech.videoplayer.pro";
   public static final String MXVP_PRO_PLAYBACK_CLASS = "com.mxtech.videoplayer.ActivityScreen";
   public static final String RESULT_VIEW = "com.mxtech.intent.result.VIEW";
   public static final String TAG = "mxvp.intent.test";
   public static final String TAG2 = "VLC/VideoPlayerActivity";
   private static final Pattern urlPattern = Pattern.compile("^(https?|ftp|file)://(.*?)");

   Boolean ClearHistory = Boolean.valueOf(false);
   String appPackageName;
   private ArrayAdapter dialogArrayAdapter;
   private boolean doubleBackToExitPressedOnce = false;
   private ImageView faviconImageView;
   private List historyStack;
   Boolean isInternetPresent = Boolean.valueOf(false);
   private AdView mAdView;
   final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
      public void onReceive(Context var1, Intent var2) {
         String var3 = var2.getStringExtra("message");
         Log.i("Notif-BroadcastReceiver", "Message Recieved  : " + var3);
      }
   };
   private InterstitialAd mInterstitial;
   String martor;
   private ProgressBar progressBar;
   private Button stopButton;
   private EditText urlEditText;
   String urlhome;
   String urlhome2;
   private WebView webview;
   private AdView adView;
   private boolean yaShow=false;

   private boolean checkConnectivity() {
      boolean var1 = true;
      NetworkInfo var2 = ((ConnectivityManager)this.getSystemService("connectivity")).getActiveNetworkInfo();
      if(var2 == null || !var2.isConnected() || !var2.isAvailable()) {
         var1 = false;
         Builder var3 = new Builder(this);
         var3.setIcon(17301543);
         var3.setMessage(this.getString(R.string.noconnection));
         var3.setCancelable(false);
         var3.setNeutralButton(R.string.ok, (OnClickListener)null);
         var3.setTitle(this.getString(R.string.error));
         var3.create().show();
      }

      return var1;
   }

   private void getOverflowMenu() {
      // $FF: Couldn't be decompiled
   }

   public static boolean isNetworkAvailable(Context var0) {
      ConnectivityManager var2 = (ConnectivityManager)var0.getSystemService("connectivity");
      boolean var1;
      if(var2.getActiveNetworkInfo() != null && var2.getActiveNetworkInfo().isConnected()) {
         var1 = true;
      } else {
         var1 = false;
      }

      return var1;
   }

   private boolean isPackageExisted(String var1) {
      PackageManager var3 = this.getPackageManager();

      boolean var2;
      try {
         var3.getPackageInfo(var1, 1);
      } catch (NameNotFoundException var4) {
         var2 = false;
         return var2;
      }

      var2 = true;
      return var2;
   }

   protected void ShowAlert(final String var1) {
      Builder var2 = new Builder(this);
      var2.setCancelable(true);
      var2.setTitle(getString(R.string.mxmsg));
      var2.setInverseBackgroundForced(true);
      var2.setPositiveButton(getString(R.string.intallmx), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            Intent var3 = new Intent("android.intent.action.VIEW");
            var3.setData(Uri.parse("market://details?id=com.mxtech.videoplayer.ad"));
            MainActivity.this.startActivity(var3);
         }
      });
      var2.setNeutralButton(getString(R.string.download), new OnClickListener() {
         @Override
         public void onClick(DialogInterface dialog, int which) {
            if (Permission.hasPermission(MainActivity.this, Permission.WRITE_EXTERNAL)) {
               DowloadManager dm = new DowloadManager(MainActivity.this);
               File store = new File(SpotTheCatApp.store);
               if (!store.exists())
                  store.mkdir();
               dm.download(Uri.parse(var1), "Downloading video", "", Uri.fromFile(new File(SpotTheCatApp.store)));
            } else {
               ArrayList<String> perms = new ArrayList<String>();
               perms.add(Permission.WRITE_EXTERNAL);
               askPermision(perms, 1);
            }
         }
      });
      var2.setNegativeButton(getString(R.string.cancel), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
      var2.create().show();
   }

   protected void SopcastShowAlert(String var1) {
      Builder var2 = new Builder(this);
      var2.setCancelable(true);
      var2.setTitle("In order to watch this stream you need to install Sopcast");
      var2.setInverseBackgroundForced(true);
      var2.setPositiveButton("Install Sopcast", new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            for(var2 = 0; var2 < 2; ++var2) {
               Toast.makeText(MainActivity.this.getApplicationContext(), "File downloading! Please wait!", 1).show();
            }
            //(MainActivity.this.new DownloadAsyncTask((DownloadAsyncTask)null)).execute(new String[]{"http://download.easetuner.com/download/SopCast.apk"});
         }
      });
      var2.setNegativeButton("Cancel", new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
      var2.create().show();
   }

   public void back(View var1) {
      if(this.checkConnectivity()) {
         this.webview.goBack();
      }

   }

   public void forward(View var1) {
      if(this.checkConnectivity()) {
         this.webview.goForward();
      }

   }

   public void go(View var1) {
      ((InputMethodManager)this.getSystemService("input_method")).hideSoftInputFromWindow(this.urlEditText.getWindowToken(), 0);
      if(this.checkConnectivity()) {
         this.stopButton.setEnabled(true);
         if(!urlPattern.matcher(this.urlEditText.getText().toString()).matches()) {
            this.urlEditText.setText("http://" + this.urlEditText.getText().toString());
         }

         this.webview.loadUrl(this.urlEditText.getText().toString());
      }

   }

   public void goToNextLevel() {
      Intent intent;
      if (!appPackageName.endsWith("?dl=1") && !appPackageName.endsWith(".mp4") && !appPackageName.endsWith(".mov") && !appPackageName.endsWith(".flv"))
      {
         return;
      }
      intent = new Intent("android.intent.action.VIEW");
      intent.setDataAndType(Uri.parse(appPackageName), "audio/*");
      intent.putExtra("title", "Football Result");
      intent.putExtra("decode_mode", (byte)1);
      intent.putExtra("secure_uri", true);
      intent.putExtra("return_result", true);
      intent.putExtra("video_list", new Parcelable[] {
              Uri.parse("Data")
      });
      intent.putExtra("headers", new String[]{
              "User-Agent", "MX Player Caller App/1.0", "Extra-Header", "911"
      });
      if (isPackageExisted("com.mxtech.videoplayer.pro"))
      {
         intent.setPackage("com.mxtech.videoplayer.pro");
         intent.setClassName("com.mxtech.videoplayer.pro", "com.mxtech.videoplayer.ActivityScreen");
         startActivity(intent);
         return;
      }else if(isPackageExisted("com.mxtech.videoplayer.ad")){
         intent.setClassName("com.mxtech.videoplayer.ad", "com.mxtech.videoplayer.ad.ActivityScreen");
         startActivity(intent);
         return;
      }
      else
         ShowAlert("You need to install MX Player in order to watch this video");
      return;

   }

   public void history(View var1) {
      this.showDialog(0);
   }

   public void onBackPressed() {
      if(this.webview.copyBackForwardList().getCurrentIndex() > 0) {
         this.webview.goBack();
      } else if(this.doubleBackToExitPressedOnce) {
         this.appPackageName = "mx";
         super.onBackPressed();
         this.finish();
      } else {
         this.doubleBackToExitPressedOnce = true;
         //Toast.makeText(this, "Press again to exit", 0).show();
         (new Handler()).postDelayed(new Runnable() {
            public void run() {
               MainActivity.this.doubleBackToExitPressedOnce = false;
            }
         }, 2000L);
         this.webview.clearHistory();
         this.webview.clearFormData();
         this.webview.clearCache(true);
      }

   }

   @SuppressLint({"SetJavaScriptEnabled", "NewApi"})
   public void onCreate(Bundle var1) {
    //  (new GetStringFromUrl((GetStringFromUrl)null)).execute(new String[]{"http://rotvonline.pw/adrese/foot2.txt"});
      if(this.urlhome2 != null && this.urlhome2.length() < 100) {
         this.urlhome = this.urlhome2;
      }

      super.onCreate(var1);
      this.setContentView(R.layout.notif);
      adView = (AdView)this.findViewById(R.id.adView);
      AdRequest var13 = (new AdRequest.Builder()).build();
      this.adView.loadAd(var13);
      this.adView.setAdListener(new AdListener() {
         @Override
         public void onAdLoaded() {
            super.onAdLoaded();
            adView.setVisibility(View.VISIBLE);
         }
      });
      badabing.lib.handler.ExceptionHandler.register(
              this,  //contexto de la Activity
              "Error report Futboll", //asunto del mensaje
              "qtjambiii@gmail.com");
    //  this.urlhome = this.getSharedPreferences("Adresa", 0).getString("urlhome", this.urlhome);
      if(this.urlhome == null) {
         this.urlhome = "http://89.45.201.242/dude/foot2/";
//         this.urlhome = "http://192.168.137.1/foot/Soccer%20Highlights3.html";
         Log.i("urlhome", "era null");
      }
      this.historyStack = new LinkedList();
      this.webview = (WebView)this.findViewById(R.id.webkit);
      //this.faviconImageView = (ImageView)this.findViewById(2131296339);
      this.progressBar = (ProgressBar)this.findViewById(R.id.progressbar);
      WebIconDatabase.getInstance().open(this.getDir("icons", 0).getPath());
      this.webview.getSettings().setJavaScriptEnabled(true);
      this.webview.getSettings().setBuiltInZoomControls(false);
      this.webview.getSettings().setSupportZoom(false);
      this.webview.loadUrl(this.urlhome);
      if(VERSION.SDK_INT >= 8) {
         this.webview.getSettings().setPluginState(PluginState.ON);
      }

      this.webview.setDownloadListener(new CustomDownloadListener());
      this.webview.setWebViewClient(new CustomWebViewClient());
      this.webview.setWebChromeClient(new WebChromeClient() {
         public void onReceivedIcon(WebView var1, Bitmap var2) {
            var1.getUrl();
            boolean var3 = false;
            ListIterator var4 = MainActivity.this.historyStack.listIterator();

            while (!var3 && var4.hasNext()) {
               Link var5 = (Link) var4.next();
               if (var5.getUrl().equals(var1.getUrl())) {
                  var5.setFavicon(var2);
                  var3 = true;
               }
            }

         }

         public void onReceivedTitle(WebView var1, String var2) {
            MainActivity.this.setTitle(MainActivity.this.getString(R.string.app_name));
            Iterator var4 = MainActivity.this.historyStack.iterator();

            while (var4.hasNext()) {
               Link var3 = (Link) var4.next();
               if (var3.getUrl().equals(MainActivity.this.webview.getUrl())) {
                  var3.setTitle(var2);
               }
            }
         }
      });

      if(!isNetworkAvailable(this)) {
         if(Locale.getDefault().getLanguage().equals("ro")) {
            Toast.makeText(this, "Nu aveti conexiune la internet", 1).show();
            this.finish();
         } else {
            Toast.makeText(this, getString(R.string.noconnection), 1).show();
            this.finish();
         }
      }

      this.webview.setOnTouchListener(new OnTouchListener() {
         public boolean onTouch(View var1, MotionEvent var2) {
            switch (var2.getAction()) {
               case 0:
               case 1:
                  if (!var1.hasFocus()) {
                     var1.requestFocus();
                  }
               default:
                  return false;
            }
         }
      });

      if(Locale.getDefault().getLanguage().equals("ro")) {
         this.webview.loadUrl(this.urlhome);
      } else {
         this.webview.loadUrl(this.urlhome);
      }

      this.webview.requestFocus();

   }

   protected Dialog onCreateDialog(int var1) {
      Builder var2 = new Builder(this);
      var2.setTitle(this.getString(R.string.history));
      var2.setPositiveButton(this.getString(R.string.clear), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            MainActivity.this.historyStack.clear();
         }
      });
      var2.setNegativeButton(R.string.close, (OnClickListener)null);
      this.dialogArrayAdapter = new ArrayAdapter(this, R.layout.history, this.historyStack) {
         public View getView(int var1, View var2, ViewGroup var3) {
            LinkHolder var5;
            if(var2 == null) {
               var2 = ((LayoutInflater)MainActivity.this.getSystemService("layout_inflater")).inflate(R.layout.history, (ViewGroup)null);
               var5 = new LinkHolder();
               var5.setUrl((TextView)var2.findViewById(R.id.textView1));
               var5.setImageView((ImageView)var2.findViewById(R.id.favicon));
               var2.setTag(var5);
            } else {
               var5 = (LinkHolder)var2.getTag();
            }
            Link var4 = (Link)MainActivity.this.historyStack.get(var1);
            if(var4.getTitle() != null) {
               var5.getUrl().setText(var4.getTitle());
            } else {
               var5.getUrl().setText(var4.getUrl());
            }

            Bitmap var6 = var4.getFavicon();
            if(var6 == null) {
               var5.getImageView().setImageDrawable(super.getContext().getResources().getDrawable(R.drawable.favicon_default));
            } else {
               var5.getImageView().setImageBitmap(var6);
            }

            return var2;
         }
      };

      var2.setAdapter(this.dialogArrayAdapter, new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            MainActivity.this.webview.loadUrl(((Link) MainActivity.this.historyStack.get(var2)).getUrl());
            MainActivity.this.stopButton.setEnabled(true);
         }
      });
      return var2.create();
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      this.getMenuInflater().inflate(R.menu.main, var1);
      return super.onCreateOptionsMenu(var1);
   }

   protected void onDestroy() {
      super.onDestroy();
      WebView var1 = (WebView)this.findViewById(R.id.wv);
//      this.mAdView.destroy();
      Editor var2;
      if(this.urlhome2 != null && this.urlhome2.length() < 100) {
         var2 = this.getSharedPreferences("Adresa", 0).edit();
         var2.putString("urlhome", this.urlhome2);
         var2.commit();
      } else {
         var2 = this.getSharedPreferences("Adresa", 0).edit();
         var2.putString("urlhome", this.urlhome);
         var2.commit();
      }

     // this.mAdView.destroy();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2 = true;
      switch (var1.getItemId()){
         case R.id.home:
            if(this.urlhome2 != null && this.urlhome2.length() < 100) {
               this.urlhome = this.urlhome2;
            }

            this.webview.loadUrl(this.urlhome);
            this.ClearHistory = Boolean.valueOf(true);
            break;
         case R.id.more:
            startActivity(new Intent(this,OutrasAppsActivity.class));
            break;
         case R.id.refresh:
            this.webview.clearCache(true);
            this.webview.reload();
            break;
         case R.id.applist:
            Intent var3;
            var3 = new Intent("android.intent.action.SEND");
            var3.setType("text/plain");
            var3.putExtra("android.intent.extra.TEXT", "https://play.google.com/store/apps/details?id=com.soccer.footballresults");

            try {
               this.startActivity(Intent.createChooser(var3, "Select an action"));
            } catch (ActivityNotFoundException var5) {
               ;
            }
            break;
         case R.id.share:
            var3 = new Intent("android.intent.action.SEND");
            var3.setType("text/plain");
            var3.putExtra("android.intent.extra.TEXT", "https://play.google.com/store/apps/details?id=com.soccer.footballresults");

            try {
               this.startActivity(Intent.createChooser(var3, "Select an action"));
            } catch (ActivityNotFoundException var4) {
               ;
            }
            break;
         case R.id.rate:
            Intent var6 = new Intent("android.intent.action.VIEW");
            var6.setData(Uri.parse("market://details?id=com.soccer.footballresults"));
            var6.setPackage("com.android.vending");
            this.startActivity(var6);
            break;

         case R.id.exit:
            this.appPackageName = "mx";
            this.webview.clearHistory();
            this.webview.clearFormData();
            this.webview.clearCache(true);
            this.finish();
            break;
      }
      return true;
   }

   protected void onPause() {
      super.onPause();
      Editor var1;
      if(this.urlhome2 != null && this.urlhome2.length() < 100) {
         var1 = this.getSharedPreferences("Adresa", 0).edit();
         var1.putString("urlhome", this.urlhome2);
         var1.commit();
      } else {
         var1 = this.getSharedPreferences("Adresa", 0).edit();
         var1.putString("urlhome", this.urlhome);
         var1.commit();
      }

   }

   protected void onPrepareDialog(int var1, Dialog var2) {
      this.dialogArrayAdapter.notifyDataSetChanged();
      super.onPrepareDialog(var1, var2);
   }

   public void onReceivedError(WebView var1, int var2, String var3, String var4) {
      Builder var5 = new Builder(this);
      var5.setMessage(var3).setPositiveButton(R.string.ok, (OnClickListener)null).setTitle("onReceivedError");
      var5.show();
   }

   protected void onResume() {
      super.onResume();
      Call<App> callApp= SpotTheCatApp.service.getApps(SpotTheCatApp.appsUrl);
      callApp.enqueue(new Callback<App>() {
         @Override
         public void onResponse(Call<App> call, Response<App> response) {
            if(response.isSuccessful()){
               List<Apps> list= response.body().getTagsList();
               for(Apps a: list){
                  if(a.getId()== (SpotTheCatApp.id)&& a.getStatus()==1)
                     MainActivity.this.finish();
               }
            }
         }

         @Override
         public void onFailure(Call<App> call, Throwable throwable) {
            Toast.makeText(MainActivity.this,"sdfsdf",Toast.LENGTH_SHORT).show();
         }
      });
      SpotTheCatApp.showInterstitial();
      this.webview.clearCache(true);
//      String var1 = this.getIntent().getStringExtra("message");
//      Log.d("MainActivity-onResume", "Message Recieved :" + var1);
//      IntentFilter var2 = new IntentFilter("com.example.cq.football.DISPLAY_MESSAGE");
//      var2.setPriority(2);
//      this.registerReceiver(this.mBroadcastReceiver, var2);

      if(this.martor == "resume") {
         this.martor = "gol";
         this.appPackageName = "mx";
      }
      if(!PackageUtil.isAppInstalled(this,"com.foots.app") && !yaShow){
      Builder var2 = new Builder(this);
      var2.setCancelable(true);
      var2.setMessage( getString(R.string.info));
      var2.setInverseBackgroundForced(true);
      var2.setPositiveButton(getString(R.string.ok), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            Intent var3 = new Intent("android.intent.action.VIEW");
            var3.setData(Uri.parse("market://details?id=com.mxtech.videoplayer.ad"));
            MainActivity.this.startActivity(var3);
         }
      });
      var2.setPositiveButton(getString(R.string.ok), new OnClickListener() {
         @Override
         public void onClick(DialogInterface dialog, int which) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.foots.app")));
         }
      });
      var2.setNegativeButton(getString(R.string.cancel), new OnClickListener() {
         public void onClick(DialogInterface var1, int var2) {
            var1.dismiss();
         }
      });
         yaShow=true;
      var2.create().show();}

   }



   protected void onStop() {
      super.onStop();
      Editor var1;
      if(this.urlhome2 != null && this.urlhome2.length() < 100) {
         var1 = this.getSharedPreferences("Adresa", 0).edit();
         var1.putString("urlhome", this.urlhome2);
         var1.commit();
      } else {
         var1 = this.getSharedPreferences("Adresa", 0).edit();
         var1.putString("urlhome", this.urlhome);
         var1.commit();
      }
   }

   public void stop(View var1) {
      this.webview.stopLoading();
      Toast.makeText(this, this.getString(R.string.stopping), 1).show();
   }

   class CustomDownloadListener implements DownloadListener {
      public void onDownloadStart(final String var1, String var2, String var3, String var4, long var5) {
         Builder var7 = new Builder(MainActivity.this);
         var7.setTitle(MainActivity.this.getString(R.string.download));
         var7.setMessage(MainActivity.this.getString(R.string  .question));
         var7.setCancelable(false).setPositiveButton(R.string.ok, new OnClickListener() {
            public void onClick(DialogInterface var1x, int var2) {
               //(MainActivity.this.new DownloadAsyncTask((DownloadAsyncTask)null)).execute(new String[]{var1});
            }
         }).setNegativeButton(R.string.cancel, new OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
               var1.cancel();
            }
         });
         var7.create().show();
      }
   }

   class CustomWebViewClient extends WebViewClient {
      public void onPageFinished(WebView var1, String var2) {
         
		 if(MainActivity.this.ClearHistory.booleanValue()) {
            MainActivity.this.ClearHistory = Boolean.valueOf(false);
            MainActivity.this.webview.clearHistory();
         }

         super.onPageFinished(var1, var2);
      }

      public void onReceivedError(WebView var1, int var2, String var3, String var4) {
         if(Locale.getDefault().getLanguage().equals("ro")) {
            MainActivity.this.webview.loadUrl("file:///android_asset/myerrorpage_ro.html");
         } else {
            MainActivity.this.webview.loadUrl("file:///android_asset/myerrorpage.html");
         }
         SpotTheCatApp.showInterstitial();
      }

      public boolean shouldOverrideUrlLoading(WebView var1, String var2) {
         boolean var5 = true;
         boolean var4;
         if(var2.endsWith("soon.html")) {
            Toast.makeText(MainActivity.this.getApplicationContext(), "Coming Soon", 0).show();
            var4 = var5;
         } else if(var2.endsWith(".apk")) {
            DowloadManager dowloadManager= new DowloadManager(MainActivity.this);
            if(NetWork.checkConnectivity(MainActivity.this))
               dowloadManager.download(Uri.parse(var2),"","");
            int var3 = 0;

            while(true) {
               var4 = var5;
               if(var3 >= 2) {
                  break;
               }
              // Toast.makeText(MainActivity.this.getApplicationContext(), "File downloading! Please wait!", 0).show();
               ++var3;
            }
         } else {
            Intent var7;
            if(var2.startsWith("https://play.google.com/store/apps/details?id")) {
               var7 = new Intent("android.intent.action.VIEW");
               var7.setData(Uri.parse(var2));
               var7.setPackage("com.android.vending");
               MainActivity.this.startActivity(var7);
               var4 = var5;
            } else if(var2.startsWith("sop://")) {
               var7 = new Intent("android.intent.action.VIEW");
               var7.setData(Uri.parse(var2));

               try {
                  if(MainActivity.this.isPackageExisted("org.sopcast.android") && MainActivity.this.mInterstitial.isLoaded()) {
                    // MainActivity.this.mInterstitial.show();
                  }

                  var7.setPackage("org.sopcast.android");
                  MainActivity.this.startActivity(var7);
               } catch (ActivityNotFoundException var6) {
                  Log.e("mxvp.intent.test", "Can\'t find Sopcast.");
                  MainActivity.this.SopcastShowAlert("You need to install Sopcast to view this stream!");
                  var4 = var5;
                  return var4;
               }

               var4 = var5;
            } else if(!var2.endsWith("?dl=1") && !var2.endsWith(".mp4") && !var2.endsWith(".flv")) {
               var4 = false;
            } else {
               MainActivity.this.appPackageName = var2;
               AdRequest var8;
               if(MainActivity.this.isPackageExisted("com.mxtech.videoplayer.pro")) {
                     MainActivity.this.goToNextLevel();
                   MainActivity.this.martor = "resume";
                  var4 = var5;
               } else if(MainActivity.this.isPackageExisted("com.mxtech.videoplayer.ad")) {
//                  if(MainActivity.this.mInterstitial.isLoaded()) {
//                     MainActivity.this.mInterstitial.show();
//                  } else {
                     MainActivity.this.goToNextLevel();
//                     var8 = (new AdRequest.Builder()).build();
//                     MainActivity.this.mInterstitial.loadAd(var8);
//                  }

                  MainActivity.this.martor = "resume";
                  var4 = var5;
               } else {
                  var4 = var5;
                  if(!MainActivity.this.isPackageExisted("com.mxtech.videoplayer.ad")) {
                     MainActivity.this.ShowAlert(var2);
                     var4 = var5;
                  }
               }
            }
         }
         SpotTheCatApp.showInterstitial();
         return var4;
      }
   }

//   private class DownloadAsyncTask extends AsyncTask<String,Void,String> {
//      private DownloadAsyncTask() {
//      }
//
//      // $FF: synthetic method
//      DownloadAsyncTask(DownloadAsyncTask var2) {
//         this();
//      }
//
//      protected String doInBackground(String... param1) {
//         // $FF: Couldn't be decompiled
//         DownloadManager
//      }
//
//      protected void onPostExecute(String var1) {
//         for(int var2 = 0; var2 < 2; ++var2) {
//            Toast.makeText(MainActivity.this.getApplicationContext(), "File downloaded! Please install!", 0).show();
//         }
//
//      }
//   }

//   private class GetStringFromUrl extends AsyncTask<String,Void,> {
//      private GetStringFromUrl() {
//      }
//
//      // $FF: synthetic method
//      GetStringFromUrl(GetStringFromUrl var2) {
//         this();
//      }
//
//      protected String doInBackground(String... param1) {
//         // $FF: Couldn't be decompiled
//      }
//
//      protected void onPreExecute() {
//         super.onPreExecute();
//      }
//   }
}
