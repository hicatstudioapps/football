package com.example.cq.football;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by CQ on 24/04/2016.
 */
public interface IService {

    @GET
    Call<App> getApps(@Url String url);

}
