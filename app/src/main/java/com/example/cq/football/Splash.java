package com.example.cq.football;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.soccer.footballresults.R;


public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_splash);
        ((TextView)findViewById(R.id.text)).setTypeface(SpotTheCatApp.bold);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this,MainActivity.class));
                finish();
            }
        },2000);
//        YoYo.with(Techniques.BounceIn).interpolate(new AccelerateDecelerateInterpolator()).duration(1000).delay(500).playOn(findViewById(R.id.imageView));
    }

}
